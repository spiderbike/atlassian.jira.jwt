﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Atlassian.Jira.OAuth;
using Atlassian.Jira.Remote;
using Atlassian.Jira;
using Microsoft.IdentityModel.Tokens;
using RestSharp;
using RestSharp.Authenticators;

namespace Atlassian.Jira
{



    public class JwtAuthenticator : IAuthenticator
    {
        private readonly string _issuer;
        private readonly string _audience;
        private readonly string _sharedSecret;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="issuer">if a connect app, the addon key</param>
        /// <param name="audience">if a connect app, the client key</param>
        /// <param name="basePath"></param>
        /// <param name="sharedSecret"></param>
        public JwtAuthenticator(string issuer, string audience, string sharedSecret)
        {
            _issuer = issuer;
            _audience = audience;
            _sharedSecret = sharedSecret;
        }

        public void Authenticate(IRestClient client, IRestRequest request)
        {
            var requestUri = client.BuildUri(request);
            var path = requestUri.AbsolutePath;

            if (!path.StartsWith("/"))
                path = "/" + path;

            var qsh = CalculateHash(request.Method.ToString(), path,
                requestUri.Query.TrimStart('?'));


            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateEncodedJwt(new SecurityTokenDescriptor
            {
                Issuer = _issuer,
                Audience = _audience,
                Claims = new Dictionary<string, object>
                {
                    ["qsh"] = qsh
                },
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_sharedSecret)), SecurityAlgorithms.HmacSha256Signature)
            });

            request.AddHeader("Authorization", $"JWT {token}");
        }

        public static string GenerateCanonicalRequest(string method, string path, string queryString = "")
        {
            var result = new StringBuilder();

            // method
            result.Append(method.ToUpperInvariant());

            // path
            result.Append("&");
            if (String.IsNullOrEmpty(path))
            {
                path = "/";
            }
            if (!path.StartsWith("/"))
            {
                path = "/" + path;
            }
            result.Append(path);

            // query string
            var queryBuilder = new UriBuilder("https", "localhost") { Query = queryString };

            var queryStringParamCollection = HttpUtility.ParseQueryString(queryBuilder.Uri.Query);
            
            var canonicalQueryString = String.Join("&", Enumerable.Range(0, queryStringParamCollection.Count)
                .Select(i => (Key: queryStringParamCollection.GetKey(i), Value: queryStringParamCollection.Get(i).Split(',')))
                .Where(kvp => kvp.Key != "jwt")
                .OrderBy(x => x.Key)
                .Select(x =>
                {
                    return String.Format("{0}={1}", x.Key,
                        String.Join(",", x.Value
                            .OrderBy(i => i)
                            .Select(EscapeUriDataStringRfc3986)));
                }));

            result.Append("&");
            if (!String.IsNullOrEmpty(canonicalQueryString))
            {
                result.Append(canonicalQueryString);
            }

            return result.ToString();
        }



        public static string CalculateHash(string method, string path, string queryString)
        {
            return CalculateHash(GenerateCanonicalRequest(method, path, queryString));
        }

        private static string CalculateHash(string canonicalRequest)
        {
            using (var sha = SHA256.Create())
            {
                var computedHash = sha.ComputeHash(Encoding.UTF8.GetBytes(canonicalRequest));
                return BitConverter.ToString(computedHash).Replace("-", "").ToLower();
            }
        }

        // from http://stackoverflow.com/questions/846487/how-to-get-uri-escapedatastring-to-comply-with-rfc-3986
        /// <summary>
        /// The set of characters that are unreserved in RFC 2396 but are NOT unreserved in RFC 3986.
        /// </summary>
        private static readonly string[] UriRfc3986CharsToEscape = new[] { "!", "*", "'", "(", ")" };

        /// <summary>
        /// Escapes a string according to the URI data string rules given in RFC 3986.
        /// </summary>
        /// <param name="value">The value to escape.</param>
        /// <returns>The escaped value.</returns>
        /// <remarks>
        /// The <see cref="Uri.EscapeDataString"/> method is <i>supposed</i> to take on
        /// RFC 3986 behavior if certain elements are present in a .config file.  Even if this
        /// actually worked (which in my experiments it <i>doesn't</i>), we can't rely on every
        /// host actually having this configuration element present.
        /// </remarks>
        internal static string EscapeUriDataStringRfc3986(string value)
        {
            // Start with RFC 2396 escaping by calling the .NET method to do the work.
            // This MAY sometimes exhibit RFC 3986 behavior (according to the documentation).
            // If it does, the escaping we do that follows it will be a no-op since the
            // characters we search for to replace can't possibly exist in the string.
            StringBuilder escaped = new StringBuilder(Uri.EscapeDataString(value));

            // Upgrade the escaping to RFC 3986, if necessary.
            for (int i = 0; i < UriRfc3986CharsToEscape.Length; i++)
            {
                escaped.Replace(UriRfc3986CharsToEscape[i], Uri.HexEscape(UriRfc3986CharsToEscape[i][0]));
            }

            // Return the fully-RFC3986-escaped string.
            return escaped.ToString();
        }
    }

    public class JiraJwtRestClient : JiraRestClient
    {
        public static Jira CreateJwtRestClient(string url, string addonKey, string clientKey, string sharedSecret, JiraRestClientSettings settings = null)
        {
            settings = settings ?? new JiraRestClientSettings();
            var restClient = new JiraJwtRestClient(url, addonKey, clientKey, sharedSecret, settings);

            return Jira.CreateRestClient(restClient, settings.Cache);
        }

        public JiraJwtRestClient(
            string url,
            string addonKey,
            string clientKey,
            string sharedSecret,
            JiraRestClientSettings settings)
            : base(
                url,
                new JwtAuthenticator(addonKey, clientKey, sharedSecret),
                settings)
        { }
    }
}
