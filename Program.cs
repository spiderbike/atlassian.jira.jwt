﻿using System;
using System.Linq;

namespace Atlassian.Jira.Jwt
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");


            var slayTheDragonClientKey = Environment.GetEnvironmentVariable("SlayTheDragonClientKey");
            var slayTheDragonAddonKey = Environment.GetEnvironmentVariable("SlayTheDragonAddonKey");
            var slayTheDragonBaseUrl = Environment.GetEnvironmentVariable("SlayTheDragonBaseUrl");
            var slayTheDragonSharedSecret = Environment.GetEnvironmentVariable("SlayTheDragonSharedSecret");

            var jiraJwt = JiraJwtRestClient.CreateJwtRestClient(slayTheDragonBaseUrl, slayTheDragonAddonKey, slayTheDragonClientKey, slayTheDragonSharedSecret);

           var issues = from i in jiraJwt.Issues.Queryable
                where i.Priority == "Medium" && i.Reporter == "Mark Richardson"
                orderby i.Created
                select i;
            foreach (var issue in issues)
            {
                Console.WriteLine("JWT: " + issue.Key);
            }

            Console.ReadKey();
        }
    }
}
